namespace Md5Roulette.Logic
{
    public interface INumberGenerator
    {
        float GetNumber(int minNumber, int maxNumber);
    }
}